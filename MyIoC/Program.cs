﻿using System;
using System.Reflection;

namespace MyIoC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var container = new Container();

            // Register dependencies using attributes.
            container.AddAssembly(Assembly.GetExecutingAssembly());

            var customerBLL = (CustomerBLL)container.CreateInstance(typeof(CustomerBLL));
            var customerBLL2 = container.CreateInstance<CustomerBLL>();

            var logger = (Logger)container.CreateInstance(typeof(Logger));
            var customerDAL = container.CreateInstance<ICustomerDAL>();

            var contractBLL = (ContractBLL)container.CreateInstance(typeof(ContractBLL));
            var contractDLL = container.CreateInstance<ContractDLL>();

            // Reset containers' settings.
            container.Reset();

            // Register dependencies manually.
            container.AddType(typeof(CustomerBLL));
            container.AddType(typeof(Logger));
            container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
            container.AddType(typeof(ContractBLL));
            container.AddType(typeof(ContractDLL));

            customerBLL = (CustomerBLL)container.CreateInstance(typeof(CustomerBLL));
            customerBLL2 = container.CreateInstance<CustomerBLL>();

            logger = (Logger)container.CreateInstance(typeof(Logger));
            customerDAL = container.CreateInstance<ICustomerDAL>();

            contractBLL = (ContractBLL)container.CreateInstance(typeof(ContractBLL));
            contractDLL = container.CreateInstance<ContractDLL>();

            Console.WriteLine("Hello World!");
        }
    }
}
