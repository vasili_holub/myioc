﻿using System.Collections.Generic;
using System.Linq;

namespace MyIoC.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection) where T : class
        {
            if (collection == null)
            {
                return true;
            }

            return !collection.Any();
        }
    }
}
