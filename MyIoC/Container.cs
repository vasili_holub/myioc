﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MyIoC.Extensions;

namespace MyIoC
{
	public class Container
	{
		private readonly Dictionary<Type, Type> _registration = new Dictionary<Type, Type>();

		private IEnumerable<Type> _importConstructorTypes; // Collection of types decoratated with ImportConstructor attribute.
		private IEnumerable<Type> _exportTypes; // Collection of types decoratated with Export attribute.
		private IEnumerable<Type> _importTypes; // Collection of types containing properties decorated with import attribute.


		public void AddAssembly(Assembly assembly)
		{
			var types = assembly.GetTypes().Where(t => t.IsClass || t.IsInterface); // Collection of assembly types and interfaces.

			_importConstructorTypes = types.Where(t => t.GetCustomAttribute(typeof(ImportConstructorAttribute)) != null);
			_exportTypes = types.Where(t => t.GetCustomAttribute(typeof(ExportAttribute)) != null);
			_importTypes = types.Where(type => type.GetProperties().Any(p => Attribute.IsDefined(p, typeof(ImportAttribute)))).ToList();

			foreach (var exportType in _exportTypes)
			{
				var exportAttributeParameter = (exportType.GetCustomAttribute(typeof(ExportAttribute)) as ExportAttribute).Contract;
				_registration.Add(exportAttributeParameter ?? exportType, exportType);
			}

			foreach (var importConstructorType in _importConstructorTypes)
			{
				_registration.Add(importConstructorType, importConstructorType);
			}

			foreach (var importType in _importTypes)
			{
				_registration.Add(importType, importType);
			}
		}

		public void AddType(Type type)
		{
			_registration.Add(type, type);
		}

		public void AddType(Type type, Type baseType)
		{
			_registration.Add(baseType, type);
		}

		public object CreateInstance(Type type)
		{
			if (HasExportAttribute(_registration[type]))
			{
				return Activator.CreateInstance(_registration[type]);
			}

			if (HasImportConstructorAttribute(type))
            {
                return Create(type);
            }

			if (HasImportAttribute(type))
			{
				var importObject = Activator.CreateInstance(type);
				foreach (var propertyType in type.GetProperties())
				{
					var propertyValue = Activator.CreateInstance(propertyType.GetType());
					propertyType.SetValue(importObject, propertyValue);

				}

				return importObject;
			}

			if (!type.IsAbstract)
			{
                return Create(type);
			}

            throw new InvalidOperationException($"No registration for type {type}");
        }

		public T CreateInstance<T>()
		{
			var type = typeof(T);
			return (T)CreateInstance(type);
		}

        public void Reset()
        {
            _registration.Clear();
		}

        private object Create(Type type)
        {
            var ctor = type.GetConstructors().Single();
            var parameterTypes = ctor.GetParameters().Select(p => p.ParameterType);
            var dependencies = parameterTypes.Select(t => CreateInstance(t)).ToArray();
            return Activator.CreateInstance(type, dependencies);
		}

        private bool HasImportAttribute(Type type)
		{
			return type != null && !_importTypes.IsNullOrEmpty() && _importTypes.Any(t => t == type);
		}

		private bool HasImportConstructorAttribute(Type type)
		{
			return type != null && !_importConstructorTypes.IsNullOrEmpty() && _importConstructorTypes.Any(t => t == type);
		}

		private bool HasExportAttribute(Type type)
		{
			return type != null && !_exportTypes.IsNullOrEmpty() && _exportTypes.Any(t => t == type);
		}

		public void Sample()
		{
			var container = new Container();
			container.AddAssembly(Assembly.GetExecutingAssembly());

			var customerBLL = (CustomerBLL)container.CreateInstance(typeof(CustomerBLL));
			var customerBLL2 = container.CreateInstance<CustomerBLL>();

			container.AddType(typeof(CustomerBLL));
			container.AddType(typeof(Logger));
			container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
		}
	}
}
