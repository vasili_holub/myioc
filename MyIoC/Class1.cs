﻿namespace MyIoC
{
	[ImportConstructor]
	public class CustomerBLL
	{
		public ICustomerDAL Dal { get; set; }

		public Logger Logger { get; set; }

		public CustomerBLL(ICustomerDAL dal, Logger logger)
		{
			Dal = dal;
			Logger = logger;
		}
	}

	public class CustomerBLL2
	{
		[Import]
		public ICustomerDAL CustomerDAL { get; set; }
		[Import]
		public Logger logger { get; set; }
	}
}
